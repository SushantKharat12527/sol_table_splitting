﻿using Sol_Table_Splitting.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_EF_Table_Spliting.Entity
{
    public class UserLoginEntity:IUserLoginEntity
    {
        public int? UserId { get; set; }

        public String UserName { get; set; }

        public String Password { get; set; }
    }
}
