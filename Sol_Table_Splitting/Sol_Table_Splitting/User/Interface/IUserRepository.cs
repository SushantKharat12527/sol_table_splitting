﻿using Sol_EF_Table_Spliting.Entity;
using Sol_Table_Splitting.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_Table_Splitting.User.Interface
{
   public interface IUserRepository
    {
        Task<IEnumerable<IUserEntity>> GetUser();
    }
}
