﻿using Sol_EF_Table_Spliting.Entity;
using Sol_Table_Splitting.EF;
using Sol_Table_Splitting.Entity;
using Sol_Table_Splitting.User.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_Table_Splitting
{
    public class UserRepository:IUserRepository
    {
        #region Declaration
        private TestdbEntities db = null;
        #endregion
        #region Constructor
        public UserRepository()
        {
            db = new TestdbEntities();
        }
        #endregion
        #region Public Method
        public async Task<IEnumerable<IUserEntity>> GetUser()
        {
            try
            {
                return await Task.Run(() => {

                    var getQuery =
                    db
                    ?.tblUserAlls
                    ?.AsEnumerable()
                    ?.Select(this.SelectUser)
                    .ToList();

                    return getQuery;

                });
            }
            catch (Exception)
            {

                throw;
            }
          
        }

      
        #endregion
        #region Private Property

        public Func<tblUserAll,IUserEntity> SelectUser
        {
            get
            {
                return (leTblUserObj) => new UserEntity()
                {
                    UserId = leTblUserObj.UserId,
                    FirstName = leTblUserObj.FirstName,
                    LastName = leTblUserObj.LastName,
                    UserLogin = new UserLoginEntity()
                    {
                        UserName = leTblUserObj?.Login.UserName,
                        Password = leTblUserObj?.Login.Password
                    },
                    UserCommunication = new UserCommunicationEntity()
                    {
                        EmailId = leTblUserObj?.Communication.Email,
                        MobileNo = leTblUserObj?.Communication.MobileNo
                    }


                };

            }
        }
        #endregion
    }
}
